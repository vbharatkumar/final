package com.cmoon.batch7.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.cmoon.batch7.R;

public class SharedPrefernceActivity extends AppCompatActivity {

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_prefernce);
        preferences = getSharedPreferences("MYPREFER", MODE_PRIVATE);
        editor = preferences.edit();

    }

    public void LoadData(View view) {
        editor.putBoolean("key_boolean", true); // Storing boolean - true/false
        editor.putString("key_name", "Sridhar"); // Storing string
        editor.putString("key_email", "saisridhar123@gmail.com"); // Storing string
        editor.putString("key_surname", "saisridhar"); // Storing string
        editor.putInt("key_num", 1024); // Storing integer
        // editor.putFloat("key_name", "float value"); // Storing float
        // editor.putLong("key_name", "long value"); // Storing long

        editor.commit(); // commit changes
        Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();

        Intent i1 = new Intent(SharedPrefernceActivity.this, Shared_outputActivity.class);
        startActivity(i1);

    }

}
