package com.cmoon.batch7.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.cmoon.batch7.R;
import com.cmoon.batch7.adapter.Rv_StudentsAdp;
import com.cmoon.batch7.app.AppController;
import com.cmoon.batch7.app.CustomJsonObjectRequest;
import com.cmoon.batch7.models.Student;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class RecyclerViewActivity extends AppCompatActivity {

    String TAG = "RecyclerViewActivity";
    ProgressBar progress;
    TextView txt_norec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);
        //createData();
        progress = findViewById(R.id.progress);
        txt_norec = findViewById(R.id.txt_norec);
        SendData();
    }

    ArrayList<Student> studentsList = new ArrayList<>();

    private void createData() {

       /* Student model = new Student();
        model.setName("colourmoon");
        model.setAge(22);
        model.setImage(R.drawable.bike1);

        Student model2 = new Student();
        model2.setName("colourmoon two");
        model2.setAge(23);
        model2.setImage(R.drawable.cycle);

        studentsList.clear();
        studentsList.add(model);
        studentsList.add(model2);

        loadRecycleView();*/
    }

    private void SendData() {

        //progress.setVisibility(View.VISIBLE);
        showProgressDialog();

        String url = AppController.API_SERVER_URL + "get_categories";
        HashMap<String, String> params = new HashMap<>();
        params.put("", "");

        Log.d(TAG, url);
        Log.d(TAG, params.toString());

        CustomJsonObjectRequest request = new CustomJsonObjectRequest(Request.Method.POST, url, params,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        // TODO Auto-generated method stub
                        try {

                            //progress.setVisibility(View.GONE);
                            hideProgressDialog();

                            Log.d(TAG, "RESPONSE : " + response.toString());
                            String result = response.getString("err_code");

                            if (result.equals("valid")) {
                                studentsList.clear();

                                JSONObject object = response.getJSONObject("data");
                                JSONArray array = object.getJSONArray("category");

                                for (int i = 0; i < array.length(); i++) {
                                    Student model = new Student();
                                    JSONObject jsonObject = array.getJSONObject(i);
                                    model.setID(jsonObject.getString("id"));
                                    model.setName(jsonObject.getString("name"));
                                    model.setAge(jsonObject.getString("num_count"));
                                    model.setImage(jsonObject.getString("image"));
                                    studentsList.add(model);
                                }
                                txt_norec.setVisibility(View.GONE);
                            } else {
                                alertMsgBox("There is problem with this ID");
                                txt_norec.setVisibility(View.VISIBLE);
                            }

                            loadRecycleView();
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        // progress.setVisibility(View.GONE);
                        hideProgressDialog();
                        // TODO Auto-generated method stub
                        if (error instanceof TimeoutError) {
                            Toast.makeText(RecyclerViewActivity.this, "Request Time out error. Your internet connection is too slow to work", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(RecyclerViewActivity.this, "Connection Server error", Toast.LENGTH_LONG).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(RecyclerViewActivity.this, "Network connection error! Check your internet Setting", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(RecyclerViewActivity.this, "Parsing error", Toast.LENGTH_LONG).show();
                        }

                    }
                });
        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);
    }

    private void loadRecycleView() {
        RecyclerView recycle = findViewById(R.id.recycle);
        //  LinearLayoutManager layoutManager_ver = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        //  LinearLayoutManager layoutManager_hor = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        GridLayoutManager layoutManager_grid = new GridLayoutManager(this, 1);
        recycle.setLayoutManager(layoutManager_grid);
        Rv_StudentsAdp rv_studentsAdp = new Rv_StudentsAdp(this, studentsList);
        recycle.setAdapter(rv_studentsAdp);
    }

    ProgressDialog progressDialog;

    public void showProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("loading");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    public void hideProgressDialog() {

        if (progressDialog != null)
            progressDialog.dismiss();
    }

    public void alertMsgBox(String message) {
        new AlertDialog.Builder(this)
                .setTitle(message)
                .setMessage(null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

}
