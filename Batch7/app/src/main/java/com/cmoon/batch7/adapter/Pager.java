package com.cmoon.batch7.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.cmoon.batch7.fragments.Tab2;
import com.cmoon.batch7.fragments.Tab3;
import com.cmoon.batch7.fragments.Tab4Fragment;
import com.cmoon.batch7.fragments.TabFragment;

/**
 * Created by Sridhar on 02-06-2017.
 */

public class Pager extends FragmentStatePagerAdapter {
    //integer to count number of tabs
    private int tabCount;

    //Constructor to the class
    public Pager(FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new TabFragment();
            case 1:
                return new Tab2();
            case 2:
                return new Tab3();
            case 3:
                return new Tab4Fragment();

            default:

                return null;
        }
    }


    @Override
    public int getCount() {
        return tabCount;
    }

}