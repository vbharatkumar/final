package com.cmoon.batch7.fireBaseNotifi;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by sridhar on 05-02-2018.
 */

public class FirebaseIDService extends MyFirebaseMessagingService {

    private String TAG = "MY_FIRE_BASE_INSTANT_ID_SERVICE";

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshedtoken: " + token);
        sendRegistrationToServer(token);
    }

    private void sendRegistrationToServer(String token) {

    }


}
