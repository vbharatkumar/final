package learnesay.lreanfrombasic.gui;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TabHost;

import learnesay.lreanfrombasic.R;


public class TabInsideDialogboxActivity extends AppCompatActivity {

    private Dialog dialog;
    private int width, height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_inside_dialogbox);
        Display display = this.getWindowManager().getDefaultDisplay();
        width = display.getWidth();
        height = display.getHeight();

        Button btnShowDialogbox =(Button)findViewById(R.id.btn_showpopup);
        btnShowDialogbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                dialog= new Dialog(TabInsideDialogboxActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.dialogfragment);
                dialog.getWindow().setLayout((int) (width*0.70), (int) (height*0.40));
                dialog.show();

           /*// getSupportFragmentManager().beginTransaction().replace(R.id.container12, new TodayTimingsFragment()).commit();
            Button btnToday=(Button)dialog.findViewById(R.id.btnToday);
            btnToday.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    getSupportFragmentManager().beginTransaction().replace(R.id.container12, new TodayTimingsFragment()).commit();
                }
            });
            Button btnTomorrow=(Button)dialog.findViewById(R.id.btnTomorrow);
            btnTomorrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    getSupportFragmentManager().beginTransaction().replace(R.id.container12, new TomorrowTimingsFragment()).commit();
                }
            });*/


                //Assign id to Tabhost.
                TabHost TabHostWindow = (TabHost)dialog.findViewById(android.R.id.tabhost);
                TabHostWindow.setup();
                //Creating tab menu.
                TabHost.TabSpec TabMenu1 = TabHostWindow.newTabSpec("First tab");
                TabHost.TabSpec TabMenu2 = TabHostWindow.newTabSpec("Second Tab");

                //Setting up tab 1 name.
                TabMenu1.setIndicator("Tab1").setContent(new Intent(TabInsideDialogboxActivity.this,TodayTimingActivity.class));
                TabMenu2.setIndicator("Tab2").setContent(new Intent(TabInsideDialogboxActivity.this,TomorrowtimingActivity.class));

                TabHostWindow.addTab(TabMenu1);
                TabHostWindow.addTab(TabMenu2);


                TabHostWindow.getTabWidget().getChildAt(0).setBackgroundColor(Color.parseColor("#3399FF")); //selected
                TabHostWindow.getTabWidget().getChildAt(1).setBackgroundColor(Color.parseColor("#ADD6FF")); //unselected
            }

        });
    }


}
