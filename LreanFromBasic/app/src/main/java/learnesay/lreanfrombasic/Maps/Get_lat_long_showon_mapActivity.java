package learnesay.lreanfrombasic.Maps;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import learnesay.lreanfrombasic.R;

public class Get_lat_long_showon_mapActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private GoogleApiClient mGoogleApiClient;
    private final static int REQUEST_PERMISSION = 9;
    private String TAG = "Map_Lat & Long";
    private Location mLastLocation;
    TextView mLatitudeText, mLongitudeText;
    Button get_location;

    private double currentLatitude, currentLongitude;

    private static LatLng lat_lng;
    private Marker marker;
    private GoogleMap mMap;
    SupportMapFragment supportMapFragment;
    String city;

    List<Address> addresses = null;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_lat_long_showon_map);

        mLatitudeText = (TextView) findViewById(R.id.textView4);
        mLongitudeText = (TextView) findViewById(R.id.textView5);
        get_location = (Button) findViewById(R.id.getlocation);

        ex();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSION);
            return;
        } else {
            ex();
        }

        get_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*to reload the map again*/
                reFreshmap();
            }
        });

    }


    private void ex() {
        if (isLocationEnabled(Get_lat_long_showon_mapActivity.this)) {
            buildGoogleApiClient();
        } else {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivityForResult(intent, 0);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 0) {
            if (isLocationEnabled(Get_lat_long_showon_mapActivity.this)) {
                buildGoogleApiClient();
                Toast.makeText(this, "Location Was Enabled", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Location Was Not Enabled", Toast.LENGTH_SHORT).show();
            }
        }

    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }

    }


    private void buildGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    // The next two lines tell the new client that “this” current class will handle connection stuff
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    //fourth line adds the LocationServices API endpoint from GooglePlayServices
                    .addApi(LocationServices.API)
                    .build();

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }

        Log.d(TAG, "onStop: Disconnected from Google API Client ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Now lets connect to the API
        if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
            Log.d(TAG, "onResume: connected to Google API Client");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
            Log.d(TAG, "onStart: connected to Google API Client ");
        }
    }


    /**
     * Dispatch onPause() to fragments.
     */
    @Override
    protected void onPause() {
        super.onPause();

        Log.v(this.getClass().getSimpleName(), "onPause()");

        if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()) {
            //Disconnect from API onPause()
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                mGoogleApiClient.disconnect();
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "onConnected: ");
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Log.d(TAG, "onConnected: permissions were not given manually,so terminating from onconnecter");
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLastLocation != null) {
            Log.d(TAG, "onConnected: entered into if");
            currentLatitude = mLastLocation.getLatitude();
            currentLongitude = mLastLocation.getLongitude();

            lat_lng = new LatLng(currentLatitude, currentLongitude);


            mLongitudeText.setText("\nLati    :" + currentLatitude + "\nLong: " + currentLongitude);
            Toast.makeText(this, "lat: " + currentLatitude + "Long: " + currentLongitude, Toast.LENGTH_LONG).show();

            Geocoder geocoder = new Geocoder(Get_lat_long_showon_mapActivity.this, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(currentLatitude, currentLongitude, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            String dno = addresses.get(0).getAddressLine(0);
            String address = addresses.get(0).getAddressLine(1);
            city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String zip = addresses.get(0).getPostalCode();
            String country = addresses.get(0).getCountryName();
            // Toast.makeText(this, dno+" \n "+address+" \n  "+city+" \n "+state+" \n  "+zip+" \n "+country, Toast.LENGTH_SHORT).show();

            mLatitudeText.setText(dno + "\n" + address + "\n" + city + "\n" + state + "\n" + zip + "\n" + country);
            // mLongitudeText.setText(""+city+"," +state);

            /*To fetch loaction for every 5 seconds*/
            LocationRequest mLocationRequest = LocationRequest.create();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setInterval(3000); // Update location every second
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            /*fetch loaction Ends*/

            /*Loading map after getting latitude and longitude */
            FragmentManager fragmentManager = this.getSupportFragmentManager();
            supportMapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.map);
            reFreshmap();

        } else {
            Log.d(TAG, "onConnected: entered into else");
            mLatitudeText.setText("Location not available");
            mLongitudeText.setText("Location not available");
        }

    }


    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended: ");

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:");

    }

    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
        mLatitudeText.setText("" + currentLatitude);

        mLongitudeText.setText("\nLati    :" + currentLatitude + "\nLong: " + currentLongitude);

        String dno = addresses.get(0).getAddressLine(0);
        String address = addresses.get(0).getAddressLine(1);
        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String zip = addresses.get(0).getPostalCode();
        String country = addresses.get(0).getCountryName();
        mLatitudeText.setText(dno + "\n" + address + "\n" + city + "\n" + state + "\n" + zip + "\n" + country);

        reFreshmap();
        //Toast.makeText(this, currentLatitude + " Location Changed " + currentLongitude + "", Toast.LENGTH_LONG).show();

    }

    public void reFreshmap() {
        supportMapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        marker = mMap.addMarker(new MarkerOptions()
                .position(lat_lng)
                .title(city)
                .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.drawable.ic_delivery_motorbike))));
               // .icon(BitmapDescriptorFactory.fromResource(R.drawable.googleg_color)));
               // .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
        marker.setTag(0);


        float zoomLevel = (float) 18.0; //This goes up to 21
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lat_lng, zoomLevel));
        mMap.setOnMarkerClickListener(this);

        /*final LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(marker.getPosition());
        final LatLngBounds bounds = builder.build();

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 15));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(16));
            }
        });*/

    }

    private Bitmap getMarkerBitmapFromView(@DrawableRes int resId) {

        View customMarkerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.view_custom_marker, null);
        ImageView markerImageView = (ImageView) customMarkerView.findViewById(R.id.img_marker);
        markerImageView.setImageResource(resId);
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);
        return returnedBitmap;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        mMap.moveCamera(CameraUpdateFactory.newLatLng(lat_lng));
        float zoomLevel = (float) 20.0; //This goes up to 21
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lat_lng, zoomLevel));
        return false;
    }
}
