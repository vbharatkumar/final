package learnesay.lreanfrombasic.multimedia;

import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import learnesay.lreanfrombasic.R;

public class VideoActivity extends AppCompatActivity {
    VideoView videoView1;
    Button btn_start;

    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        videoView1=(VideoView)findViewById(R.id.v1);
        videoView1.setVisibility(View.GONE);
         btn_start=(Button)findViewById(R.id.start);
        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                btn_start.setVisibility(View.GONE);
                videoView1.setVisibility(View.VISIBLE);
                String video_name="sample";
                String uriPath = "android.resource://"+getPackageName()+"/raw/"+ video_name;
               // String uriPath = "http://basolutions.in//m4d//uploads//videos//sample1.mp4";
                Uri UrlPath=Uri.parse(uriPath);
                videoView1.setVideoURI(UrlPath);
                MediaController mediaController = new MediaController(VideoActivity.this);
                mediaController.setAnchorView(videoView1);
                videoView1.setMediaController(mediaController);
                videoView1.requestFocus();
                videoView1.setVideoURI(UrlPath);
                videoView1.start();

                videoView1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                        videoView1.setVisibility(View.GONE);
                        btn_start.setVisibility(View.VISIBLE);
                    }
                });
            }
        });

    }

    @Override
    public void onBackPressed() {
        videoView1.stopPlayback();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        videoView1.setVisibility(View.GONE);
        btn_start.setVisibility(View.VISIBLE);
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}
