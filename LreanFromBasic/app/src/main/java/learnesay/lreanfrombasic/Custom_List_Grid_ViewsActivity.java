package learnesay.lreanfrombasic;

import android.content.Context;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class Custom_List_Grid_ViewsActivity extends AppCompatActivity {

    ArrayList<String> title_list;
    ArrayList<Integer> img_list;
    private ListView list1;
    GridView gridview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom__list__grid__views);

        title_list=new ArrayList<String>();
        img_list=new ArrayList<Integer>();

        title_list.add("Title 1");
        title_list.add("Title 2");
        title_list.add("Title 3");
        title_list.add("Title 4");
        title_list.add("Title 1");
        title_list.add("Title 2");
        title_list.add("Title 3");
        title_list.add("Title 4");

        img_list.add(R.drawable.a1);
        img_list.add(R.drawable.a2);
        img_list.add(R.drawable.a3);
        img_list.add(R.drawable.a4);
        img_list.add(R.drawable.a1);
        img_list.add(R.drawable.a2);
        img_list.add(R.drawable.a3);
        img_list.add(R.drawable.a4);

        list1=(ListView)findViewById(R.id.list) ;
        list1.setAdapter(new customadapter());

        gridview=(GridView)findViewById(R.id.gridview);
        gridview.setAdapter(new customadapter_grid());


    }

    private class customadapter implements ListAdapter {
        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        @Override
        public boolean isEnabled(int position) {
            return false;
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public int getCount() {
            return title_list.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inf = (LayoutInflater) Custom_List_Grid_ViewsActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inf.inflate(R.layout.custom_list_layout, null);


                ImageView img1=(ImageView)convertView.findViewById(R.id.imgview1);
                TextView txt1=(TextView)convertView.findViewById(R.id.txt1);
                img1.setImageResource(img_list.get(position));
                txt1.setText(title_list.get(position));


            }

            return convertView;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public int getViewTypeCount() {
            return title_list.size();
        }

        @Override
        public boolean isEmpty() {
            return false;
        }
    }

    private class customadapter_grid implements ListAdapter {
        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        @Override
        public boolean isEnabled(int position) {
            return false;
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public int getCount() {
            return img_list.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inf = (LayoutInflater) Custom_List_Grid_ViewsActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inf.inflate(R.layout.custom_grid_layout, null);

            }

            ImageView img1=(ImageView)convertView.findViewById(R.id.imgview1);
            TextView txt1=(TextView)convertView.findViewById(R.id.txt1);
            img1.setImageResource(img_list.get(position));
            txt1.setText(title_list.get(position));


            return convertView;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public int getViewTypeCount() {
            return img_list.size();
        }

        @Override
        public boolean isEmpty() {
            return false;
        }
    }
}
