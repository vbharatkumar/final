package learnesay.lreanfrombasic.etc;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import learnesay.lreanfrombasic.R;

import static android.Manifest.permission.CALL_PHONE;

public class CallActivity extends AppCompatActivity {

    String ph_strng = "";
    EditText ph;
    static final int callphone = 16;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);

        ph = (EditText) findViewById(R.id.ph);
        Button call = (Button) findViewById(R.id.call);

        if (ContextCompat.checkSelfPermission(this, CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {
            // We do not have this permission. Let's ask the user
            ActivityCompat.requestPermissions(this, new String[]{CALL_PHONE}, callphone);
        }


        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ph_strng = ph.getText().toString();
               // call();
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", ph_strng, null));
               startActivity(intent);
            }
        });

    }
    public void call() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + ph_strng));
        if (ActivityCompat.checkSelfPermission(this, CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Toast.makeText(this, "Permissions Were not there", Toast.LENGTH_SHORT).show();
            return;
        }
        startActivity(callIntent);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case callphone: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted!
                    // you may now do the action that requires this permission
                } else {
                    // permission denied
                }
                return;
            }

            // other 'switch' lines to check for other
            // permissions this app might request
        }
    }
}