package learnesay.lreanfrombasic.tab_layout_Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import learnesay.lreanfrombasic.R;
import learnesay.lreanfrombasic.RecyclerAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class Tab4Fragment extends Fragment {


    private View rootview;

    public Tab4Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootview = inflater.inflate(R.layout.fragment_tab4, container, false);

        RecyclerView mRecyclerView =(RecyclerView)rootview.findViewById(R.id.recyclerview);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
       // int animtype = R.anim.recyclerview_list_animation;
        RecyclerAdapter adapter = new RecyclerAdapter();
        mRecyclerView.setAdapter(adapter);
        
        return rootview;
    }

}
