package learnesay.lreanfrombasic.tab_layout_Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import learnesay.lreanfrombasic.R;

public class Tab3 extends Fragment {


    private View rootview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.tab3, container, false);

        RecyclerView mRecyclerView =(RecyclerView)rootview.findViewById(R.id.recyclerview);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        int animtype = R.anim.recycler_items_fall_down;
        JobSearchResultRecyclerListAdapter adapter = new JobSearchResultRecyclerListAdapter(getActivity(),animtype);
        mRecyclerView.setAdapter(adapter);

        return rootview;

    }
}
