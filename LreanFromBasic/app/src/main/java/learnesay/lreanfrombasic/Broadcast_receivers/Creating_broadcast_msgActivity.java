package learnesay.lreanfrombasic.Broadcast_receivers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import learnesay.lreanfrombasic.R;

public class Creating_broadcast_msgActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creating_broadcast_msg);

    }

    public void broadcastIntent(View view)
    {
        Intent intent = new Intent();
        intent.setAction("com.sridhar.CUSTOM_INTENT");
        sendBroadcast(intent);
    }
}
