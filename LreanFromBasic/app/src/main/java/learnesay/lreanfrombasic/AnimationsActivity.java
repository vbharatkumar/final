package learnesay.lreanfrombasic;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Toast;

public class AnimationsActivity extends AppCompatActivity implements Animation.AnimationListener {

    Animation animrotate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animations);
        final Button btn1=(Button)findViewById(R.id.button5);

        // load animations
        animrotate = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate);
        // set animation listeners
        animrotate.setAnimationListener(this);
        // start fade in animation
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn1.startAnimation(animrotate);
            }
        });

    }

    @Override
    public void onAnimationStart(Animation animation) {
        if (animation == animrotate)
        {
            Toast.makeText(getApplicationContext(), "Animation started", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onAnimationEnd(Animation animation) {
      // check for fade in animation


    }

    @Override
    public void onAnimationRepeat(Animation animation) {
        if (animation == animrotate) {
            Toast.makeText(getApplicationContext(), "Animation Repeated", Toast.LENGTH_SHORT).show();
        }

    }
}
