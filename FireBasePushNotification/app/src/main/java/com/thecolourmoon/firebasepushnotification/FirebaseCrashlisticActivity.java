package com.thecolourmoon.firebasepushnotification;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class FirebaseCrashlisticActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // Fabric.with(this,new Crashlytics());
        setContentView(R.layout.activity_firebase_crashlistic);
        forceCrash();
    }

    public void forceCrash(){
        throw new RuntimeException("This is force crash");
    }
}
